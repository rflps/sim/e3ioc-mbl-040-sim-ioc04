
require essioc
require s7plc,1.4.1+0
require rflps_sim,0.2.0+0

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet(IOCNAME, "MBL-040RFC:Ctrl-IOC-103")
epicsEnvSet(IOCDIR, "MBL-040RFC_Ctrl-IOC-103")
epicsEnvSet(LOG_SERVER_NAME, "172.16.107.59")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# RFLPS SIM Configuration
############################################################################
epicsEnvSet("PREFIX", "$(RFLPS_PREFIX=MBL-040RFC:)")

epicsEnvSet("LOCATION", "MBL-040RFC")
epicsEnvSet("ENGINEER", "Rafael Montano <rafael.montano@ess.eu>"

#var s7plcDebug 5

## Datablocks
epicsEnvSet("TCPPORTCPU", "3000")
epicsEnvSet("PLCPORTCPU", "PLCCPU")
epicsEnvSet("INSIZECPU", "8")
epicsEnvSet("OUTSIZECPU", "4")

epicsEnvSet("TCPPORTAF", "3001")
epicsEnvSet("PLCPORTAF", "PLCAF")
epicsEnvSet("INSIZEAF", "1452")
epicsEnvSet("OUTSIZEAF", "858")

epicsEnvSet("TCPPORTDIO", "3002")
epicsEnvSet("PLCPORTDIO", "PLCDIO")
epicsEnvSet("INSIZEDIO", "972")
epicsEnvSet("OUTSIZEDIO", "162")

epicsEnvSet("TCPPORTPSU", "3003")
epicsEnvSet("PLCPORTPSU", "PLCPSU")
epicsEnvSet("INSIZEPSU", "390")
epicsEnvSet("OUTSIZEPSU", "222")

# MBL-040 Klystron 1
epicsEnvSet("PLCIP", "172.16.100.167")

s7plcConfigure("$(PLCPORTCPU)","$(PLCIP)",$(TCPPORTCPU),$(INSIZECPU),$(OUTSIZECPU),1,2500,500)
s7plcConfigure("$(PLCPORTAF)","$(PLCIP)",$(TCPPORTAF),$(INSIZEAF),$(OUTSIZEAF),1,2500,500)
s7plcConfigure("$(PLCPORTDIO)","$(PLCIP)",$(TCPPORTDIO),$(INSIZEDIO),$(OUTSIZEDIO),1,2500,500)
s7plcConfigure("$(PLCPORTPSU)","$(PLCIP)",$(TCPPORTPSU),$(INSIZEPSU),$(OUTSIZEPSU),1,2500,500)

dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsCPU.substitutions", "PREFIX=$(PREFIX), KLY=4")
dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsAF.substitutions", "PREFIX=$(PREFIX), KLY=4")
dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsDIO.substitutions", "PREFIX=$(PREFIX), KLY=4")
dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsPSU.substitutions", "PREFIX=$(PREFIX), KLY=4")


iocInit()


